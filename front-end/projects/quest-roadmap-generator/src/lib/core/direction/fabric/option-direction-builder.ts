import { Direction } from "../direction";
import { OptionDirection } from "./option-direction";
import { OptionDirectionStrategy } from "./option-direction-strategy";

export class OptionDirectionBuilder {
    private strategy: OptionDirectionStrategy;

    constructor(private direction: Direction) {
        this.strategy = new OptionDirectionStrategy();
    }

    /**
     * Return an OptionDirection according to the direction strategy
     */
    build(): OptionDirection {
        return this.strategy.createOne(this.direction);
    }
}
