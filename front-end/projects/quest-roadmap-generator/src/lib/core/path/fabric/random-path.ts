
import { PointPosition } from "projects/quest-roadmap-generator/src/public-api";
import { Status } from "../../../models/status";
import { CheckPoint } from "../../checkpoint/checkpoint";
import { CheckPointOption } from "../../checkpoint/checkpoint-option";
import { OptionDirection } from "../../direction/fabric/option-direction";
import { OptionDirectionBuilder } from "../../direction/fabric/option-direction-builder";
import { MapOption } from "../../map/map-options";
import { Path } from "../path";

/**
 * Path class that permit to generate random path and checkpoints
 */
export class RandomPath extends Path {
    options: MapOption
    start: CheckPointOption;
    end: CheckPointOption;
    points: any[];

    pointPosition: PointPosition
    // private lastPoint: CheckPointOption;
    private optionDirection: OptionDirection;
    startOptions: CheckPointOption;

    /**
     * Instanciate a new Path from a Map of rand-svg-path-generator
     * @param {MapOption} options
     */
    constructor(scene: Phaser.Scene, options: MapOption, optionDirectionBuilder: OptionDirectionBuilder) {
        super(scene, options);

        this.options = options;

        this.optionDirection = optionDirectionBuilder.build();
        this.startOptions = this.optionDirection.getStartOptions(this.options);

        this.defineStart(this.startOptions.label, this.startOptions.type, this.startOptions.x, this.startOptions.y, this.startOptions.childrens, this.startOptions.status);
        this.defineEnd("End", "end", null, null, [], Status.locked);
    }

    init(): void { }

    preload(): void { }

    create(): void {
        this.path = new Phaser.Curves.Path(this.start.x, this.start.y).splineTo(this.points.map(checkpoint => { return new Phaser.Math.Vector2(checkpoint.x, checkpoint.y) }));

        var graphics = this.scene.add.graphics();
        graphics.lineStyle(this.styles.strokeWidth, this.styles.fill, this.styles.strokeAlpha);

        this.path.draw(graphics, 128);

        this.points.map((checkpoint, id) => {
            let checkpointLabel = checkpoint.label

            if (!checkpoint.label) {
                checkpointLabel = ({
                    "start": checkpoint.label = this.start.label,
                    "end": checkpoint.label = this.end.label
                })[checkpoint.type] || checkpoint.label;
            }

            checkpoint.label = checkpointLabel;

            let cp = new CheckPoint(this.scene, this.options, { x: checkpoint.x, y: checkpoint.y }, checkpoint.label, checkpoint.type, checkpoint.status);
            cp.draw();

            this.checkpoints.push(cp);

            if (id === 0 && !this.playerPosition) {
                this.playerPosition = { x: cp.position.x, y: cp.position.y - 70 };
            }
        });

        this.add(graphics);
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map using trigo
     */
    protected doGeneratePathPoints(): void {
        const opt = this.options;

        let options = {
            min: opt.margin.top + (opt.checkpoints.radius + 25),
            max: opt.sizing.height + opt.margin.top,
            maxAlpha: opt.checkpoints.randomizer.maxAlpha,
            distance: opt.checkpoints.randomizer.distance
        }

        this.points[0].x = this.start.x;

        this.points[0].y = this.start.y;

        if (!this.points[0].status) {
            this.start.status;
        }

        let last_point = this.points[0];

        this.points.forEach(checkpoint => {
            const point = this.generatePointPosition(checkpoint, last_point, options);

            checkpoint.x = point.x;
            checkpoint.y = point.y;

            last_point = checkpoint;

            if (this.getChildrenCount(checkpoint)) {
                checkpoint.childrens.forEach(children => {
                    const point = this.generatePointPosition(children, last_point, options);

                    children.x = point.x;
                    children.y = point.y;

                    last_point = children;
                });
            }
        });
    }


    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (min | max | alpha { minAlpha | maxAlpha } | distance)
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        const alphaRange = { min: 0, max: options.maxAlpha };

        const alpha: number = Math.random() * (alphaRange.max - alphaRange.min) + alphaRange.min;

        if (!point.x) {
            point.x = previousPoint.x + (Math.cos(alpha * Math.PI / 180) * options.distance);
        }

        if (!point.y) {
            const direction = Math.round(Math.random()) ? 1 : -1;

            const randomY: number = this.generateRandomY(previousPoint, alpha, options.distance, direction);

            point.y = randomY;
        }

        const verifiedPoint = this.verifyPointPositions(options.min, options.max, point, previousPoint);

        point.x = Math.round(verifiedPoint.x);
        point.y = Math.round(verifiedPoint.y);

        return point;
    }

    /**
     * Generate random y point position
     * @param {CheckPointOption} previousPoint
     * @param {number} alpha
     * @param {number} distance
     * @param {number} direction
     * 
     * @returns {number} randomY
     */
    private generateRandomY(previousPoint: CheckPointOption, alpha: number, distance: number, direction: number): number {
        const randomY: number = previousPoint.y + (Math.sin(alpha * Math.PI / 180) * distance * direction);

        return randomY;
    }

    /**
     * Verify min and max y positions
     * @param {number} min
     * @param {number} max
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * 
     * @returns {CheckPointOption} point
     */
    verifyPointPositions(min: number, max: number, point: CheckPointOption, previousPoint: CheckPointOption): CheckPointOption {
        if (!point.y) {
            point.y = this.start.y;

            if (this.start.y < min) {
                point.y = min;
            } else if (this.start.y > max) {
                point.y = max;
            }
        } else {
            if (point.y < min) {
                point.y = min;
            } else if (point.y > max) {
                point.y = max;
            }

            if (!point.x) {
                point.x = previousPoint.x + Math.sqrt(Math.pow(this.options.checkpoints.randomizer.distance, 2) - Math.pow(Math.abs(point.y - previousPoint.y), 2));
            }
        }

        return point;
    }
}
