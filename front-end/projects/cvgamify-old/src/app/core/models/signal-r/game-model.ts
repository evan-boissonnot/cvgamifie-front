import { QuestModel } from "./quest-model";

/**
 * One game model, with quests
 */
export abstract class GameModel {
    /** Id of the game */
    id: number;
    
    /** Created date of the game, from one user */
    createdDate: Date;

    /** Title of the game */
    title: string;

    /** Description of the game */
    description: string;

    /**
     * List of quests of the game
     */
    quests: QuestModel[];

    /**
     * Current state of the goal
     */
     status: string;
}