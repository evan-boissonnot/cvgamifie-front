import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Game } from '../../../core/models/game';

import * as SignalR from '@microsoft/signalr';

/**
 * Service to get all data from api, about game
 */
@Injectable()
export class GameService {
  public gameModelBehaviorSubject: BehaviorSubject<Game> = new BehaviorSubject<Game>(null);
  public gameUpdateSubject: BehaviorSubject<Game> = new BehaviorSubject<Game>(null);

  //#region Constructors
  constructor(public hubConnection: SignalR.HubConnection, private httpClient: HttpClient) { }
  //#endregion


  //#region Public methods
  public startConnection(): Observable<any> {
    return from(this.hubConnection.start());
  }

  public addUpdateGameListener(): void {
    this.hubConnection.on('gameupdate', (data: Game) => {
      this.gameModelBehaviorSubject.next(data);
    });
  }

  /**
   * Selects one game by its id
   * @param id number to find a game
   */
  public selectOnResume(id: number): Observable<Game> {
    return this.httpClient.get<Game>(`${environment.apis.games.url}?id=${id}`);
  }

  public postNewGame(game: Game): Observable<Object> {
    return this.httpClient.post(environment.apis.games.url, game);
  }
  //#endregion
}
