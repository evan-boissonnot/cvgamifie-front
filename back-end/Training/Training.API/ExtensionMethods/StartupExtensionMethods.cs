﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using AutoMapper;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training.API;
using TrainingCourses.Domain.TrainingCourseAggregate;
using TrainingCourses.Infrastructure.Repositories;
using TrainingCourses.Infrastructure.Mappers.Profiles;
using TrainingCourses.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace TrainingCourses.API.ExtensionMethods
{
    /// <summary>
    /// Adding methods to organize startup
    /// </summary>
    public static class StartupExtensionMethods
    {
        #region Public methods
        public static IServiceCollection AddCustomMVC(this IServiceCollection services)
        {
            services.AddControllers();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            return services;
        }

        public static IServiceCollection AddMappers(this IServiceCollection services)
        {
            MapperConfiguration configuration = new MapperConfiguration(config =>
            {
                config.AddProfile(new DefaultProfile());
            });
            IMapper mapper = configuration.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }

        public static IServiceCollection AddCQRS(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Startup));
            return services;
        }

        public static IServiceCollection AddCustomDatabaseContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TrainingCourseDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("CvGamify.TrainingCourse"));
            });

            return services;
        }

        public static IServiceCollection AddDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Training.API", Version = "v1" });
            });

            return services;
        }

        public static IServiceCollection AddDependencyInjections(this IServiceCollection services)
        {
            services.AddTransient<ITrainingCourseRepository, TrainingCourseRepository>();

            return services;
        }
        #endregion
    }
}
