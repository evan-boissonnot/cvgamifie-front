﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;
using TrainingCourses.Infrastructure;

namespace TrainingCourses.Application.UnitTests
{
    public class SharedDataBaseFixture : IDisposable
    {
        #region Fields
        private TrainingCourseDbContext _context = null;
        private Mock<IMediator> _mediator = new Mock<IMediator>();
        #endregion

        #region Constructors
        public SharedDataBaseFixture()
        {
        }
        #endregion

        #region Public methods
        public TrainingCourseDbContext GetContext()
        {
            if (this._context == null)
            {
                DbContextOptionsBuilder<TrainingCourseDbContext> builder = new DbContextOptionsBuilder<TrainingCourseDbContext>();
                builder.UseInMemoryDatabase("TrainingCourse");
                this._context = new TrainingCourseDbContext(builder.Options, this._mediator.Object);

                this.Seed(this._context);
            }

            return this._context;
        }

        public void Dispose() { }
        #endregion

        #region Internal methods
        private void Seed(TrainingCourseDbContext context)
        {
            context.TrainingCourses.Add(new TrainingCourse(1, "title 1", "subtitle 1", "description 1", 1));
            context.TrainingCourses.Add(new TrainingCourse(2, "title 2","subtitle 2" , "description 2", 1));
            context.TrainingCourses.Add(new TrainingCourse(3, "title 3","subtitle 3" , "description 3", 2));

            context.SaveChanges();
        }
        #endregion
    }
}
