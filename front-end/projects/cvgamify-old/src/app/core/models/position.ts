/**
 * One position of one quest or goal
 */
export interface Position {
    /** 
     * Id of the goal 
     * */
    x: number;

    /**
     * Date of the creation of the goal
     */
    y: number;
}
