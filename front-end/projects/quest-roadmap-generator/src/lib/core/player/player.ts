import { Styles } from "../styles/styles";
import { PathPointOption } from "../path/path-point-option";
import styles from "../styles/styles-options";

/**
 * CheckPoint class permit to generate players
 */
export class Player extends Phaser.GameObjects.Container {
    pos: PathPointOption;
    styles: Styles['player'];

    icon: Phaser.GameObjects.Image

    constructor(scene: Phaser.Scene, position: PathPointOption) {
        super(scene);
        
        this.scene.add.existing(this);

        this.pos = position;

        this.styles = styles.player;
    }
    
    /**
     * Generate player icon from on a rand-svg-path-generator Path on his specified position
     */
    draw(): void {
        this.icon = this.scene.add.image(this.pos.x, this.pos.y, 'player-icon');
        this.icon.setDisplaySize(this.styles.assets.icon.iconSize.x, this.styles.assets.icon.iconSize.y);

        this.add([this.icon]);
    }

    updatePlayer(position: PathPointOption): void {
        this.icon.x = position.x;
        this.icon.y = position.y;
    }
}
