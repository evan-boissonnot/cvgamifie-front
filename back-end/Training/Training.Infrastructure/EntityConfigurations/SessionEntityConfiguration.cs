﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.Infrastructure.EntityConfigurations
{
    public class SessionEntityConfiguration : IEntityTypeConfiguration<Session>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.ToTable("Session");

            builder.HasKey(o => o.Id);
            builder.Ignore(b => b.DomainEvents);

            builder.Property<int>("SectionId")
                   .IsRequired();
        }
        #endregion
    }
}
