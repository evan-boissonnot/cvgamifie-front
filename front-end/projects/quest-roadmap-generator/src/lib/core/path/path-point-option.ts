/**
 * Option to configure a path point
 */
 export interface PathPointOption {
    x: number;
    y: number;
}
