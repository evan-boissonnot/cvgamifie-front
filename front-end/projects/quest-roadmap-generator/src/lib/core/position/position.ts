/**
 * Option to configure a path point
 */
 export interface Position {
    x: number;
    y: number;
}
