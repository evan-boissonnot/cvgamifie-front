import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireDatabase, AngularFireDatabaseModule, SnapshotAction } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import * as SignalR from '@microsoft/signalr';
import { HubConnection } from '@microsoft/signalr';
import { Status } from 'projects/cvgamify-old/src/app/core/models/status';
import { LoggerService } from 'projects/cvgamify-old/src/app/shared/services/logger/logger.service';
import { AngularFireModule } from 'projects/quest-roadmap-generator/node_modules/@angular/fire/angular-fire';
import { GameMapDataService, InteractionsService, PhaserGameComponent, QuestRoadmapGeneratorService } from 'quest-roadmap-generator';
import { Observable, of } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Game } from '../../../../core/models/game';
import { GameService } from '../../../../shared/services/game/game.service';
import { RoadMapComponent } from './road-map.component';

describe('RoadMapComponent (former)', () => {
  let component: RoadMapComponent;
  let fixture: ComponentFixture<RoadMapComponent>;

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  let gameService: GameService;
  let questRoadmapGeneratorService: QuestRoadmapGeneratorService;
  let gameMapDataService: GameMapDataService;

  const fakeAngularFirebase = {
    list: function(list: string) {
      return {
        snapshotChanges(): Observable<SnapshotAction<any>[]> {
          return of(this.actions)
        }
      }
    }
  };

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        RoadMapComponent,
        PhaserGameComponent
      ],
      imports: [
        HttpClientTestingModule,
        AngularFireDatabaseModule
      ],
      providers: [
        GameService,
        GameMapDataService,
        LoggerService,
        QuestRoadmapGeneratorService,
        InteractionsService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              id: getGameWith1CheckPoint().id,
            })
          }
        },
        { provide: HubConnection, useValue: new SignalR.HubConnectionBuilder().withUrl(environment.apis.hubs.student.url).build() },
        { provide: AngularFireDatabase, useValue: fakeAngularFirebase }
      ]
    }).compileComponents();

    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
    gameService = TestBed.inject(GameService);
    questRoadmapGeneratorService = TestBed.inject(QuestRoadmapGeneratorService);
    gameMapDataService = TestBed.inject(GameMapDataService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have roadmap element', () => {
    component.game = getGameWith3CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    expect(compiled.querySelector('phaser-game')).toBeTruthy();
  });

  it('should call the right api to get one game', () => {
    const gameToReturn = getGameWith1CheckPoint();

    fixture.detectChanges();

    let results = { param: 'id', value: gameToReturn.id };
    let url = `${environment.apis.games.url}?${results.param}=${results.value}`;

    const testRequest = httpMock.expectOne(url);
    expect(testRequest.request.method).toBe('GET');

    testRequest.flush(gameToReturn);
  });

  it('should render game title', () => {
    component.game = getGameWith2CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    const title = compiled.querySelector('h1#title');

    expect(title).toBeTruthy();
  });

  it('should render roadmap elements', () => {
    component.game = getGameWith2CheckPoint();

    fixture.detectChanges();
    const compiled = fixture.nativeElement;

    const title = compiled.querySelector('canvas');

    expect(title).toBeTruthy();
  });

  function getGameWith1CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.todo,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith2CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        },
        {
          id: 2,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.locked,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.locked,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith3CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.finished,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.finished,
              winnedXP: 100
            }
          ]
        },
        {
          id: 2,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        },
        {
          id: 3,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.locked,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.locked,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }
});
