/**
* Different checkpoint types
*/
export enum CheckpointType {
   quest = "quest",
   goal = "goal"
}