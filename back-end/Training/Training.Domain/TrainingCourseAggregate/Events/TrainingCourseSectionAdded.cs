﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate.Events
{
    /// <summary>
    /// Raised when a new section is added inside the training course
    /// </summary>
    public class TrainingCourseSectionAdded : INotification
    {
        #region Constructors
        public TrainingCourseSectionAdded(int id, string title, string description, TrainingCourse owner)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.Owner = owner;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TrainingCourse Owner { get; set; }
        #endregion
    }
}
