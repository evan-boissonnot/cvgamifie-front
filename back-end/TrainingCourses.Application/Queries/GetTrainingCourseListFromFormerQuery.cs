﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.API.Application.Models;

namespace TrainingCourses.Application.Queries
{
    /// <summary>
    /// Request a list of training courses form one former
    /// </summary>
    public class GetTrainingCourseListFromFormerQuery : IRequest<IList<TrainingCourse>>
    {
        #region Constructoes
        public GetTrainingCourseListFromFormerQuery(int formerId)
        {
            this.FormerId = formerId;
        }
        #endregion

        #region Properties
        public int FormerId { get; set; }
        #endregion
    }
}
