import { Direction } from "../../direction/direction";
import MapOptions from "../../map-settings";
import { MapOption } from "../../map/map-options";
import { OptionBuilderFabric } from "../../option-builder-fabric";
import { Path } from "../path";
import { LinearPath } from "./linear-path";
import { RandomPath } from "./random-path";

/**
 * Fabric Path class that permit to generate path and checkpoints with linear and random path
 */
export class PathFabric {

    constructor(){ }

    /**
     * Generate x and y positions for a path point 
     * @param {Phaser.Scene} scene
     * @param {"linear" | "random"} type
     * @param {MapOption} options
     * @returns {Path} path
     */
    static createPath(scene: Phaser.Scene, type: "linear" | "random", options: MapOption, direction: Direction = MapOptions.direction): any {
        const otpionDirection = OptionBuilderFabric.createOptionBuilder(direction);

        let path: Path = new LinearPath(scene, options, otpionDirection);

        if(type == "random"){
            path = new RandomPath(scene, options, otpionDirection);
        }

        return path;
    }
}
