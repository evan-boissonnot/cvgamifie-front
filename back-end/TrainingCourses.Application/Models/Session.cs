﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Application.Models
{
    /// <summary>
    /// Represents a session DTO
    /// </summary>
    public class Session: BaseDto
    {
        #region Properties
        public string Title { get; set; }
        public string Description { get; set; }
        #endregion
    }
}
