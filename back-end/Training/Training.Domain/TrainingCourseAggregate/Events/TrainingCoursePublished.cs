﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.TrainingCourseAggregate.Events
{
    /// <summary>
    /// Instance to define when a training course has been published
    /// </summary>
    public class TrainingCoursePublished : INotification
    {
        #region Constructors
        public TrainingCoursePublished(TrainingCourse item)
        {
            this.Item = item;
        }
        #endregion

        #region Properties
        public TrainingCourse Item { get; private set; }
        #endregion
    }
}
