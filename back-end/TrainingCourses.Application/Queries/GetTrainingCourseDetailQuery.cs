﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Models;

namespace TrainingCourses.API.Application.Queries
{
    public class GetTrainingCourseDetailQuery : IRequest<TrainingCourse>
    {
        #region Constructors
        public GetTrainingCourseDetailQuery(int id)
        {
            this.Id = id;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        #endregion
    }
}
