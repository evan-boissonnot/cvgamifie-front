﻿using CVGamify.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingCourses.Domain.FormerAggregate
{
    public class Former : Entity, IAggregateRoot
    {
        #region Properties
        public string Surname { get; set; }
        public string Name { get; set; }
        #endregion
    }
}
