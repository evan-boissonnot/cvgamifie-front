﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CVGamify.Framework.Exceptions
{
    /// <summary>
    /// Uses it when value if not what you have expected
    /// </summary>
    public class InvalidValueException : Exception
    {
        #region Constructors
        public InvalidValueException(Type type, string message)
            : base($"Value of {type.Name} {message}")
        {
        }
        #endregion
    }
}
