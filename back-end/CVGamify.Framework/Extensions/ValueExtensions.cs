﻿using CVGamify.Framework.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace CVGamify.Framework.Extensions
{
    /// <summary>
    /// New Methods to value objects
    /// </summary>
    public static class ValueExtensions
    {
        #region Public methods
        /// <summary>
        /// Argument not to be null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public static void MustNotBeNull<T>(this ValueObject<T> value) where T : ValueObject<T>
        {
            if (value == null)
                throw new InvalidValueException(typeof(T), "cannot be null");
        }

        /// <summary>
        /// Check if it's same type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public static void MustBe<T>(this ValueObject<T> value) where T : ValueObject<T>
        {
            if (value == null)
                throw new InvalidValueException(typeof(T), "cannot be null");
        }
        #endregion
    }
}
