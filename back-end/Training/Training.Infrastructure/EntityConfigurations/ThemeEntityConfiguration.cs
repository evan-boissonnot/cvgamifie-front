﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TrainingCourses.Domain.TrainingCourseAggregate;

namespace TrainingCourses.Infrastructure.EntityConfigurations
{
    public class ThemeEntityConfiguration : IEntityTypeConfiguration<Theme>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<Theme> builder)
        {
            builder.ToTable("Theme");

            builder.HasKey(o => o.Id);
            builder.Ignore(b => b.DomainEvents);
        }
        #endregion
    }
}
