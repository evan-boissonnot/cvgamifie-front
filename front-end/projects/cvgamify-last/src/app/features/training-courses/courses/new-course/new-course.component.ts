import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Editor } from 'primeng/editor';




@Component({
  selector: 'cvgamify-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.less']
})
export class NewCourseComponent implements OnInit, AfterViewInit {

  editable: boolean = true;
  public inputTitleInfo: string = "title";
  public inputSubtitleInfo: string = "subtitle";
  public editorInfo: string = "description";
  public editorPlaceholder: string = "Décrivez ici le contenu de la formation proposée";
  public title: string = "";
  public subtitle: string = "";
  public safeDescription: SafeHtml;
  public editorIsInvalid: boolean = false;

  createTrainingForm: FormGroup;

  constructor(private fb: FormBuilder, private readonly _sanitizer: DomSanitizer) {
    this.createTrainingForm = fb.group({
      title: new FormControl('', Validators.required),
      subtitle: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
   }

  ngOnInit(): void {
    
  }

  @ViewChild('p-editor')
  public editor: Editor;

  ngAfterViewInit(): void {
    this.editor.getQuill().scroll.domNode.addEventListener('blur', () => {
      if(this.createTrainingForm.controls['description'].invalid){
        this.editorIsInvalid = true;
      }
      else { this.editorIsInvalid = false;}
  })}

  get titleInvalid(): boolean {
    return this.elementFormIsInvalid('title');
  }

  get subtitleInvalid(): boolean {
    return this.elementFormIsInvalid('subtitle');
  }

  private elementFormIsInvalid (key: string): boolean {
    return this.createTrainingForm.controls[key].invalid && this.createTrainingForm.controls[key].touched;
  }

  

  createCourse(): void {
    this.title = this.createTrainingForm.value.title;
    this.subtitle = this.createTrainingForm.value.subtitle;
    const dirtyDescription: string = this.createTrainingForm.value.description;
    this.safeDescription = this._sanitizer.bypassSecurityTrustHtml(dirtyDescription);
    this.editable = false;
  }

  cancel(value: any = undefined): void {
    this.createTrainingForm.reset(value);
  }

  edit() {
    this.editable = true;
  }

}
