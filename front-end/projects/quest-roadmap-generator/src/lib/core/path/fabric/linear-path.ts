import { Status } from "../../../models/status";
import { CheckPoint } from "../../checkpoint/checkpoint";
import { CheckPointOption } from "../../checkpoint/checkpoint-option";
import { OptionDirection } from "../../direction/fabric/option-direction";
import { OptionDirectionBuilder } from "../../direction/fabric/option-direction-builder";
import { MapOption } from "../../map/map-options";
import { PointPositionBuilder } from "../../position/fabric/point-position-builder";
import { PointPosition } from "../../position/point-position";
import { Path } from "../path";

/**
 * Path class that permit to generate path and checkpoints
 */
export class LinearPath extends Path {
    options: MapOption;
    start: CheckPointOption;
    end: CheckPointOption;
    points: any[];

    pointPosition: PointPosition;
    private optionDirection: OptionDirection;
    startOptions;

    /**
     * Instanciate a new Path from a Map of rand-svg-path-generator
     * @param {MapOption} options
     */
    constructor(scene: Phaser.Scene, options: MapOption, optionDirectionBuilder: OptionDirectionBuilder) {
        super(scene, options);

        this.options = options;
        this.points = this.options.data;

        this.optionDirection = optionDirectionBuilder.build();
        this.startOptions = this.optionDirection.getStartOptions(this.options);

        this.defineStart(this.startOptions.label, this.startOptions.type, this.startOptions.x, this.startOptions.y, this.startOptions.childrens, this.startOptions.status);

        this.defineEnd("End", "end", null, null, [], Status.locked);
    }

    init(): void { }

    preload(): void { }

    create(): void {
        this.path = new Phaser.Curves.Path(this.start.x, this.start.y).splineTo(this.points.map(checkpoint => { return new Phaser.Math.Vector2(checkpoint.x, checkpoint.y) }));

        var graphics = this.scene.add.graphics();
        graphics.lineStyle(this.styles.strokeWidth, this.styles.fill, this.styles.strokeAlpha);

        this.path.draw(graphics, 128);

        this.points.map((checkpoint, id) => {
            let checkpointLabel = checkpoint.label

            if (!checkpoint.label) {
                checkpointLabel = ({
                    "start": checkpoint.label = this.start.label,
                    "end": checkpoint.label = this.end.label
                })[checkpoint.type] || checkpoint.label;
            }

            checkpoint.label = checkpointLabel;

            let cp = new CheckPoint(this.scene, this.options, { x: checkpoint.x, y: checkpoint.y }, checkpoint.label, checkpoint.type, checkpoint.status);
            cp.draw();
            
            this.checkpoints.push(cp);

            if (id === 0 && !this.playerPosition) {
                this.playerPosition = { x: cp.position.x, y: cp.position.y + 70 };
            }
        });

        this.add(graphics);
    }

    /**
    * Generate Path data on a rand-svg-path-generator Map
    */
    protected doGeneratePathPoints(): void {
        const opt = this.options;

        let options = {
            distance: opt.checkpoints.randomizer.distance,
            direction: opt.direction
        };

        this.points[0].x = this.start.x;

        this.points[0].y = this.start.y;

        if (!this.points[0].status) {
            this.start.status;
        }

        let last_point = this.points[0];

        this.points.forEach(checkpoint => {
            const point = this.generatePointPosition(checkpoint, last_point, options);

            checkpoint.x = point.x;
            checkpoint.y = point.y;

            last_point = checkpoint;

            if (this.getChildrenCount(checkpoint)) {
                checkpoint.childrens.forEach(children => {
                    const point = this.generatePointPosition(children, last_point, options);

                    children.x = point.x;
                    children.y = point.y;

                    last_point = children;
                });
            }
        });
    }

    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (distance | direction)
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        const pointPositionBuilder = new PointPositionBuilder(options.direction);

        this.pointPosition = pointPositionBuilder.build();

        const generatedPoint = this.pointPosition.generatePointPosition(point, previousPoint, options);

        point.x = Math.round(generatedPoint.x);
        point.y = Math.round(generatedPoint.y);

        return point;
    }
}
