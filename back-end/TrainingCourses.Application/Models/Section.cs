﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingCourses.API.Application.Models
{
    /// <summary>
    /// Represents a full section DTO
    /// </summary>
    public class Section: BaseDto
    {
        #region Properties
        public string Title { get; set; }
        public string Description { get; set; }

        public IEnumerable<Session> Sessions { get; set; }
        #endregion
    }
}
