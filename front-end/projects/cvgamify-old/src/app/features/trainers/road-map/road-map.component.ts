import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameService } from 'projects/cvgamify-old/src/app/shared/services/game/game.service';
import { Interaction } from 'projects/quest-roadmap-generator/src/lib/core/interaction/interaction';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CheckpointType } from '../../../core/models/checkpoint-type';
import { Game } from '../../../core/models/game';
import { InteractionType } from '../../../core/models/interaction-type';
import { Position } from '../../../core/models/position';
import { GameModel } from '../../../core/models/signal-r/game-model';
import { GoalModel } from '../../../core/models/signal-r/goal-model';
import { QuestModel } from '../../../core/models/signal-r/quest-model';
import { Status } from '../../../core/models/status';
import { LoggerService } from '../../../shared/services/logger/logger.service';

@Component({
  selector: 'app-road-map',
  templateUrl: './road-map.component.html',
  styleUrls: ['./road-map.component.sass']
})
export class RoadMapComponent implements OnInit, OnDestroy {
  @ViewChild('roadmap', { static: false }) el: ElementRef;
  @ViewChildren('validator') validators;

  private subscriptions: Subscription[] = [];
  private gameId = 0;
  private mapData = [];

  public gameUpdateSubject: BehaviorSubject<Game> = this.gameService.gameUpdateSubject;
  public validatorSubject: BehaviorSubject<Interaction[][]> = new BehaviorSubject<Interaction[][]>(null);
  public game: Game;
  public validatorsData: Interaction[][] = [];

  // HTTPCLient dans un service et pas ds composant
  constructor(public gameService: GameService, private route: ActivatedRoute, private logger: LoggerService) { }

  //#region Public methods
  ngOnDestroy(): void {
    this.subscriptions.forEach(item => item.unsubscribe());
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.gameId = parseInt(params.id);
    });

    const signalRSubscription = this.gameService.startConnection().subscribe(() => {
      this.logger.info('Connection started');
    }, error => {
      this.logger.error('Error while starting connection: ' + error);
    });

    const gameSubscription = this.gameService.selectOnResume(this.gameId).subscribe(item => {
      this.gameUpdateSubject.next(item);
    });

    const gameUpdateSubscription = this.gameUpdateSubject.subscribe(item => {
      this.game = item;
    });

    this.addSubscription(gameSubscription);
    this.addSubscription(gameUpdateSubscription);
    this.addSubscription(signalRSubscription);

    this.gameService.addUpdateGameListener();
  }

  //pas update game mais updateMap
  public updateGame(game: Game): void {
    this.gameService.postNewGame(game).subscribe(res => {
      this.logger.info(res);
    });

    // Array.from(this.validators).forEach((el: InteractionComponent) => {
    //   el.elementRef.nativeElement.remove();
    // });

    this.gameUpdateSubject.next(this.game);
  }

  public getMapData(mapData: any[]): void {
    this.mapData = mapData;

    this.mapData.forEach((quest, id) => {
      let questId: number = 0;

      if (id > 0 && id <= this.game.quests.length) {
        questId = this.game.quests[id - 1].id;
      }

      let validators: Interaction[] = [];

      if (id != this.mapData.length - 1) {
        if (quest.status != Status.finished) {
          this.addValidator(validators, InteractionType.validate, questId, null, CheckpointType.quest);

          if (quest.status != Status.running) {
            this.addValidator(validators, InteractionType.goto, questId, null, CheckpointType.quest);
          }

          this.addValidators(validators);
          validators = [];

          const gameQuest = this.game.quests.find(el => el.id == questId);

          if (quest.childrens) {
            quest.childrens.forEach((goal, id) => {
              let goalId: number = gameQuest.goals[id].id;

              if (goal.status != Status.finished) {
                this.addValidator(validators, InteractionType.validate, goalId, questId, CheckpointType.goal);

                if (goal.status != Status.running) {
                  this.addValidator(validators, InteractionType.goto, goalId, questId, CheckpointType.goal);
                }
              }

              this.addValidators(validators);
              validators = [];
            });
          }
        } else {
          this.addValidators(validators);

          if (quest.childrens) {
            quest.childrens.forEach(() => {
              this.addValidators(validators);
            });
          }
        }
      }
    });

    this.validatorSubject.next(this.validatorsData);
    this.validatorsData = [];
  }

  public validate($event): void {
    if ($event.checkpointType == CheckpointType.quest) {
      const quest = this.game.quests.find(el => el.id == $event.id);

      if (!quest.goals.every(goal => goal.status == Status.finished)) {
        const confirmMessage: string = 'Tous les objectif de cette quête ne sont pas validés, êtes-vous sûr de vouloir continuer ?';

        let confirm = window.confirm(confirmMessage);

        if (confirm) {
          this.updateStatus(Status.finished, quest);

          quest.goals.forEach(goal => {
            if (goal.status != Status.finished) {

              this.updateStatus(Status.locked, goal);
            }
          });
        }
      } else {
        this.updateStatus(Status.finished, quest);
      }
    }

    if ($event.checkpointType == 'goal') {
      this.game.quests.forEach((quest) => {
        if (quest.goals.find(el => el.id == $event.id)) {
          this.updateStatus(Status.finished, quest.goals.find(el => el.id == $event.id));
        }
      });
    }

    this.updateGame(this.game);
  }

  public goto($event): void {
    // this.logger.log($event);

    // Paused all possible running task
    this.game.quests.forEach(quest => {
      if (quest.status == Status.running) {
        this.updateStatus(Status.paused, quest);
      }

      if (quest.status == Status.todo) {
        if (quest.id != $event.parentId) {
          this.updateStatus(Status.paused, quest);
        }
      }

      if (quest.goals) {
        quest.goals.forEach(goal => {
          if (goal.status == Status.running) {
            this.updateStatus(Status.paused, goal);
          }
        });
      }
    });

    if ($event.checkpointType == ('quest')) {
      let quest;

      if ($event.id) {
        quest = this.game.quests.find(el => el.id == $event.id);

        this.updateStatus(Status.running, quest);

        if (quest.goals) {
          quest.goals.forEach(goal => {
            if (goal.status == Status.locked) {
              this.updateStatus(Status.todo, goal);
            }
          });
        }
      }
    }

    if ($event.checkpointType == 'goal') {
      const quest = this.game.quests.find(el => el.id == $event.parentId);
      const searchedGoal = quest.goals.find(el => el.id == $event.id);

      if (searchedGoal) {
        if (quest.status == Status.locked || quest.status == Status.paused) {
          this.updateStatus(Status.todo, quest);
        }

        quest.goals.forEach(goal => {
          if (goal.status == Status.locked) {
            this.updateStatus(Status.todo, goal);
          }
        });

        this.updateStatus(Status.running, searchedGoal);
      }
    }

    this.updateGame(this.game);
  }

  public update($event): void {
    this.logger.log($event);
  }

  public finish($event): void {
    this.logger.log($event);
  }
  //#endregion

  //#region Internal methods
  private addSubscription(subscription): void {
    this.subscriptions.push(subscription);
  }

  private addValidator(validators: Interaction[], type: InteractionType, checkpointId: number, parentCheckpointId: number, checkpointType: CheckpointType): void {
    validators.push({
      type,
      checkpointId,
      parentCheckpointId,
      checkpointType
    });
  }

  private addValidators(validators): void {
    this.validatorsData.push(validators);
  }

  private updateStatus(status: string, object: GameModel | QuestModel | GoalModel): void {
    object.status = status;
  }

  private updateTitle(title: string, object: GameModel | QuestModel | GoalModel): void {
    object.title = title;
  }

  private updateDescription(description: string, object: GameModel | QuestModel | GoalModel): void {
    object.description = description;
  }
  //#endregion
}
