import { Quest } from "./quest";

/**
 * One game model, with quests
 */
export interface Game {
    /** Id of the game */
    id: number;
    
    /** Created date of the game, from one user */
    createdDate: Date;

    /** Title of the game */
    title: string;

    /** Description of the game */
    description: string;

    /**
     * Status of the game
     */
    status: string;

    /**
     * List of quests of the game
     */
    quests: Quest[];
}
