import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrainingCourse } from 'projects/cvgamify-last/src/app/core/models/training-courses/training-course';
import { of, Observable } from 'rxjs';
import { apis } from 'projects/cvgamify-last/src/environments/apis.dev';


@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  constructor(private httpClient: HttpClient) {  }

  getAll(): Observable<TrainingCourse[]> {
    return this.httpClient.get<TrainingCourse[]>(apis.courses.url);
  }
}
