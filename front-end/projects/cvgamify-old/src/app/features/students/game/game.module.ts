import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import * as SignalR from '@microsoft/signalr';
import { HubConnection } from '@microsoft/signalr';
import { ButtonModule } from 'primeng/button';
import { environment } from 'projects/cvgamify-old/src/environments/environment';
import { QuestRoadmapGeneratorModule } from 'quest-roadmap-generator';
import { GameService } from '../../../shared/services/game/game.service';
import { GameRoutingModule } from './game-routing.module';
import { GoalComponent } from './goal/goal.component';
import { InventoryComponent } from './inventory/inventory.component';
import { QuestComponent } from './quest/quest.component';
import { RoadMapComponent } from './road-map/road-map.component';

@NgModule({
  declarations: [
    InventoryComponent,
    RoadMapComponent,
    GoalComponent,
    QuestComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule,
    ButtonModule,
    QuestRoadmapGeneratorModule
  ],
  providers: [
    GameService,
    { provide: HubConnection, useValue: new SignalR.HubConnectionBuilder().withUrl(environment.apis.hubs.student.url).build()}
  ],
  exports: [
    InventoryComponent
  ]
})
export class GameModule { }
