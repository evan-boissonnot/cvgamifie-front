// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apis: {
    games: {
      url: 'https://localhost:44378/api/game',
    },
    trainingSessions: {
      url: 'https://localhost:44378/api/trainingSession'
    },
    hubs: {
      student: {
        url: 'https://localhost:44378/hub/student'
      },
      trainer: {
        url: 'https://localhost:44378/hub/trainer'
      }
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
