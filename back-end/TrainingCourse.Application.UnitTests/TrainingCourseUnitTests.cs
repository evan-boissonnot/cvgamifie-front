﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TrainingCourses.API.Application.Commands;
using TrainingCourses.API.Application.Queries.Handlers;
using TrainingCourses.Application.Queries;
using TrainingCourses.Application.Queries.Handlers;
using TrainingCourses.Domain.TrainingCourseAggregate;
using TrainingCourses.Infrastructure;
using TrainingCourses.Infrastructure.Mappers.Profiles;
using TrainingCourses.Infrastructure.Repositories;
using Xunit;
using Xunit.Abstractions;

namespace TrainingCourses.Application.UnitTests
{
    /// <summary>
    /// Testing training course from application
    /// </summary>
    public class TrainingCourseUnitTests : IClassFixture<SharedDataBaseFixture>
    {
        #region Fields
        private TrainingCourseDbContext _context = null;
        private ITrainingCourseRepository _repository = null;
        private IMapper _mapper = null;
        private ITestOutputHelper _output = null;
        private readonly Mock<IMediator> _mediator;
        #endregion

        #region Constructors
        public TrainingCourseUnitTests(ITestOutputHelper output, SharedDataBaseFixture fixture)
        {
            this._output = output;

            this._repository = new TrainingCourseRepository(fixture.GetContext());
            this._mapper = this.PrepareMapper();
        }
        #endregion

        #region Public methods
        #region List of training courses
        [Fact]
        public async Task ShouldGetAllActiveTrainingCourseWithSuccess()
        {
            int formerId = 1;

            GetAllTrainingCourseFromOneFormerHandler handler = new GetAllTrainingCourseFromOneFormerHandler(this._repository, this._mapper);
            CancellationToken cancelToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(new GetTrainingCourseListFromFormerQuery(formerId), cancelToken);

            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
        }
        #endregion

        #region One training course
        [Fact]
        public async Task ShouldGetTrainingCourseWithSuccess()
        {
            // Arrange
            int trainingCourseId = 1;

            // Act
            GetTrainingCourseDetailHandler handler = new GetTrainingCourseDetailHandler(this._repository, this._mapper);
            CancellationToken cancelToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(new API.Application.Queries.GetTrainingCourseDetailQuery(trainingCourseId), cancelToken);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id == trainingCourseId);
        }

        [Fact]
        public async Task ShouldGetTrainingCourseWithFailedNoTrainingCourse()
        {
            // Arrange
            int trainingCourseId = 2;

            // Act
            GetTrainingCourseDetailHandler handler = new GetTrainingCourseDetailHandler(this._repository, this._mapper);
            CancellationToken cancelToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(new API.Application.Queries.GetTrainingCourseDetailQuery(trainingCourseId), cancelToken);

            // Assert
            Assert.Null(result);
        }
        #endregion

        #region Add one training course
        [Fact]
        public async void ShouldAddDraftAloneTrainingCourseWithSuccess()
        {
            // Arrange
            int formerId = 1;
            TrainingCourseDraftCommand command = new TrainingCourseDraftCommand()
            {
                Description = "Description 1",
                Subtitle = "Subtitle 1",
                Title = "Title1",
                OwnerId = formerId
            };

            // Act
            var result = await this.CallNewDraft(command);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
            Assert.True(result.OwnerId == formerId);
        }

        [Fact]
        public async void ShouldNotAddEmptyDraftAloneTrainingCourseWithFailed()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(this.CallNewEmptyDraft);
        }
        #endregion
        #endregion

        #region Internal methods
        private async Task CallNewEmptyDraft()
        {
            int formerId = 1;
            TrainingCourseDraftCommand command = new TrainingCourseDraftCommand()
            {
                Description = "",
                Subtitle = "",
                Title = "",
                OwnerId = formerId
            };

            var result = await this.CallNewDraft(command);
        }

        private async Task<API.Application.Models.TrainingCourse> CallNewDraft(TrainingCourseDraftCommand command)
        {
            TrainingCourseDraftCommandHandler handler = new TrainingCourseDraftCommandHandler(this._repository, this._mapper);
            CancellationToken cancelToken = new System.Threading.CancellationToken();
            var result = await handler.Handle(command, cancelToken);

            return result;
        }

        private IMapper PrepareMapper()
        {
            MapperConfiguration configuration = new MapperConfiguration(config =>
            {
                config.AddProfile(new DefaultProfile());
            });

            return configuration.CreateMapper();
        }
        #endregion
    }
}
