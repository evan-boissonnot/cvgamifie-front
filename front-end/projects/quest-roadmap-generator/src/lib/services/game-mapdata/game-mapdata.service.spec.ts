import { TestBed } from '@angular/core/testing';
import { AngularFireDatabase, AngularFireDatabaseModule, SnapshotAction, } from '@angular/fire/compat/database';
import { Observable, of } from 'rxjs';

import { GameMapDataService } from './game-mapdata.service';

const fakeAngularFirebase = {
  list: function(list: string) {
    return {
      snapshotChanges(): Observable<SnapshotAction<any>[]> {
        return of(this.actions)
      }
    }
  }
};

describe('GameMapDataService', () => {
  let service: GameMapDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularFireDatabaseModule
      ],
      providers: [
        GameMapDataService,
        { provide: AngularFireDatabase, useValue: fakeAngularFirebase }
      ]
    });
    service = TestBed.inject(GameMapDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
