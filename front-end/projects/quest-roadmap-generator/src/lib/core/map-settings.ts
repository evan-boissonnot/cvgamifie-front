/**
* RoadMap settings for the rand-svg-path-generator
*/

import { Direction } from "./direction/direction";
import { MapOption } from "./map/map-options";

const MapOptions: MapOption = {
  pathType: "random",
  direction: Direction.right,
  width: 0,
  height: 0,
  margin: {
    top: 60,
    right: 60,
    bottom: 60,
    left: 60
  },
  sizing: {
    width: 0,
    height: 0
  },
  checkpoints: {
    radius: 15,
    randomizer: {
      maxAlpha: 60,
      distance: 150
    }
  },
  data: [],
  successCallback: null,
  errorCallback: null
};

export default MapOptions;