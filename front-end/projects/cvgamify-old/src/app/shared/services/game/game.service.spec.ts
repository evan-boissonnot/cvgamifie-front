import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import * as SignalR from '@microsoft/signalr';
import { HubConnection } from '@microsoft/signalr';
import { environment } from 'projects/cvgamify-old/src/environments/environment';
import { Game } from '../../../core/models/game';
import { Status } from '../../../core/models/status';
import { LoggerService } from '../logger/logger.service';
import { GameService } from './game.service';

describe('GameService', () => {
  let service: GameService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        GameService,
        LoggerService,
        { provide: HubConnection, useValue: new SignalR.HubConnectionBuilder().withUrl(environment.apis.hubs.trainer.url).build() }
      ]
    });

    service = TestBed.inject(GameService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  function getGameWith1CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.todo,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith2CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        },
        {
          id: 2,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.locked,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.locked,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }

  function getGameWith3CheckPoint() {
    const game: Game = {
      id: 5,
      createdDate: new Date(),
      description: 'un jeu',
      title: 'un titre',
      status: 'started',
      quests: [
        {
          id: 1,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.finished,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.finished,
              winnedXP: 100
            }
          ]
        },
        {
          id: 2,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.todo,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.running,
              winnedXP: 100
            }
          ]
        },
        {
          id: 3,
          createdDate: new Date(),
          description: 'un jeu',
          title: 'un titre',
          status: Status.locked,
          goals: [
            {
              id: 1,
              createdDate: new Date(),
              description: 'un jeu',
              title: 'un titre',
              status: Status.locked,
              winnedXP: 100
            }
          ]
        }
      ]
    };

    return game;
  }
});
