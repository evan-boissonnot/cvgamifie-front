import { Direction } from "./direction/direction";
import { OptionDirectionBuilder } from "./direction/fabric/option-direction-builder";

/**
 * Fabric Path class that permit to generate path and checkpoints with linear and random path
 */
export class OptionBuilderFabric {

    constructor(){ }

    /**
     * Generate x and y positions for a path point 
     * @param {"linear" | "random"} type
     * @param {MapOption} options
     * @returns {Path} path
     */
    static createOptionBuilder(direction: Direction): OptionDirectionBuilder {
        let optionDirectionBuilder: OptionDirectionBuilder = new OptionDirectionBuilder(direction);

        return optionDirectionBuilder;
    }
}
