import { TestBed } from '@angular/core/testing';

import { InteractionsService } from './interactions.service';

describe('InteractionService', () => {
  let service: InteractionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
      ],
      providers: [
        InteractionsService
      ]
    });
    
    service = TestBed.inject(InteractionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
